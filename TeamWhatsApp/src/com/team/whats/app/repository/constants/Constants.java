package com.team.whats.app.repository.constants;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Constants {

    public static final String COMMA_SEPARATOR = ",";

    public static final String NEW_LINE = "\n";

    public static final Path USER_FILE_PATH = Paths.get("res/DB", "users.txt");

    public static final Path CONVERSATION_FILE_PATH = Paths.get("/Users/moholeadarius/GitFolder_IT-Home/TeamWhatsApp/DB/", "conversations.txt");

    public static final Path MESSAGE_FILE_PATH = Paths.get("/Users/moholeadarius/GitFolder_IT-Home/TeamWhatsApp/DB/", "messages.txt");

}
