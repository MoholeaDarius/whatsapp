package com.team.whats.app.repository;

import com.team.whats.app.model.User;
import com.team.whats.app.model.constants.Status;
import com.team.whats.app.repository.constants.FIleReady;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.team.whats.app.repository.constants.Constants.*;

public class UserRepository {

    private int lastId = -1;

    public List<User> get() throws IOException {
        List<User> users = new ArrayList<>();
        List<String> lines = Files.readAllLines(USER_FILE_PATH);
        if (lines.isEmpty()) {
            lastId = 0;
            return new ArrayList<>();
        }
        for (String line : lines) {
            String[] values = line.split(COMMA_SEPARATOR);
            users.add(new User(Integer.parseInt(values[0]), values[1], Status.valueOf(values[2].toUpperCase(Locale.ROOT)), values[3], values[4]));
        }
        lastId = users.get(users.size() - 1).getId();
        return users;
    }

    public User add(String name, Status status, String phoneNumber, String password) throws IOException {
        User user = new User(generateId(), name, status, phoneNumber, password);
        Files.write(USER_FILE_PATH, (FIleReady.user(user) + "\n").getBytes(), StandardOpenOption.APPEND);
        return user;
    }

    public boolean delete(int id) throws IOException {
        List<User> users = get();
        User unwantedUser = null;
        for (User user : users) {
            if (user.getId() == id) {
                unwantedUser = user;
                break;
            }
        }
        if (unwantedUser == null) {
            return false;
        }
        users.remove(unwantedUser);
        Files.deleteIfExists(USER_FILE_PATH);
        Files.createFile(USER_FILE_PATH);
        for (User user : users) {
            Files.write(USER_FILE_PATH, (FIleReady.user(user) + "\n").getBytes(), StandardOpenOption.APPEND);
        }
        return true;
    }

    public void updateStatus(int id, Status status) throws IOException {
        List<User> users = get();
        Files.deleteIfExists(USER_FILE_PATH);
        Files.createFile(USER_FILE_PATH);
        for (User user : users) {
            if (user.getId() == id){
                user.setStatus(status);
            }
            Files.write(USER_FILE_PATH, (FIleReady.user(user) + "\n").getBytes(), StandardOpenOption.APPEND);
        }
    }

    public User findUserById(int id) throws IOException {
        for (User user : get()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    private int generateId() throws IOException {
        if (lastId == -1) {
            get();
        }
        if (lastId == 0) {
            return 1;
        }
        return ++lastId;
    }

}