package com.team.whats.app.model;

import java.util.List;

public class Conversation {

    private int id;
    private String name;
    private List<Message> messages;
    private List<Integer> ids;

    public Conversation(int id, String name, List<Message> messages, List<Integer> ids) {
        this.id = id;
        this.name = name;
        this.messages = messages;
        this.ids = ids;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "Conversation{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", messages=" + messages +
                ", ids=" + ids +
                '}';
    }

}
