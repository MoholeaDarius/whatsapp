package com.team.whats.app.model;

import java.time.LocalDateTime;

public class Message {

    private int id;
    private String personName;
    private LocalDateTime messageSentDate;
    private String message;

    public Message(int id, String personName, LocalDateTime messageSentDate, String message) {
        this.id = id;
        this.personName = personName;
        this.messageSentDate = messageSentDate;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public LocalDateTime getMessageSentDate() {
        return messageSentDate;
    }

    public void setMessageSentDate(LocalDateTime messageSentDate) {
        this.messageSentDate = messageSentDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", personName='" + personName + '\'' +
                ", messageSentDate=" + messageSentDate +
                ", message='" + message + '\'' +
                '}';
    }

}
