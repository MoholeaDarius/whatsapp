package com.team.whats.app.model.constants;

public enum Status {

    ONLINE("online"),
    OFFLINE("offline"),
    UNAVAILABLE("unavailable"),
    AWAY("away");

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
