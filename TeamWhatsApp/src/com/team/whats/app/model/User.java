package com.team.whats.app.model;

import com.team.whats.app.model.constants.Status;

import java.util.List;

public class User {

    private int id;
    private String name;
    private Status status;
    private String phoneNumber;
    private String password;
    private List<Conversation> conversations;

    public User(int id, String name, Status status, String phoneNumber, String password, List<Conversation> conversations) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.conversations = conversations;
    }

    public User(int id, String name, Status status, String phoneNumber, String password) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password='" + password + '\'' +
                "}";
    }

}
