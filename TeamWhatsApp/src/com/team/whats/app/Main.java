package com.team.whats.app;

import com.team.whats.app.model.constants.Status;
import com.team.whats.app.repository.UserRepository;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        UserRepository repository = new UserRepository();
        try {
            repository.add("Test", Status.AWAY, "0707070707", "testpass");
            repository.delete(32);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
